# Test Module Drupal & Vue.JS

Docker project, encapsulating 3 containers ([Drupal 8](https://www.drupal.org/8), [MySQL](https://www.mysql.com/), [phpMyAdmin](https://www.phpmyadmin.net/))
In [Drupal](https://www.drupal.org/8) we will use two extensions to display our Vue.JS block ([Decoupled Blocks](https://www.drupal.org/project/pdb), [Decoupled Blocks: Vue.js](https://www.drupal.org/project/pdb_vue))

## Installation

First of all you have to create the files folder in drupal/sites/default. Then add write permission to this folder to all users:

```
mkdir drupal/sites/default/files
chmod a+w drupal/sites/default/files
```

You must then create the configuration and settings file which currently only exists in the form of a template. This file is drupal/sites/default/default.settings.php.

```
cp drupal/sites/default/default.settings.php drupal/sites/default/settings.php
chmod a+w drupal/sites/default/settings.php
```

## Launching the environment with docker-compose

Now that everything is ready, we go to the root of our tree structure and we execute the following command:

```
docker-compose up
```

If all goes well we can connect to the [Drupal](https://www.drupal.org/8) installer via http: // localhost: 8000 and to [phpMyAdmin](https://www.phpmyadmin.net/) via http: // localhost: 8001.
We must now create the database that will host [Drupal](https://www.drupal.org/8).

You can then go to localhost: 8000 to continue installing Drupal with the profile "Standard"

User: ** root **

Password: ** 1234 **

It is advisable to restore the rights to the conf file as follows:

```
 chmod go-w sites/default/settings.php
```


## Add Vue.JS block


In the drupal admin go to the extensions section and activate these two extensions ([link](http://localhost:8000/admin/modules))
* PDB Vue js
* Progressively Decoupled Blocks (PDB)

![](https://i.ibb.co/yQ5sDJN/Capture-d-cran-du-2021-06-22-14-34-31.png)

Now in block layout ([link](http://localhost:8000/admin/structure/block)) add a new block and select Example Block Vue.js

![](https://i.ibb.co/xFCNG7J/Capture-d-cran-du-2021-06-22-14-42-26.png)


This block was created especially to test if it is possible to easily integrate Vue.JS, it contains a simple call to an API.

To learn more about the development of a Vue.JS block the examples provided by PDB Vue js are a good base and you will find more information on the [doc](https://www.drupal.org/docs/contributed-modules/decoupled-blocks-vuejs).