new Vue({
  el: ".example-block",
  data() {
    return {
      message: "Je suis un simple block Vue.js avec un appel a une API",
      loading: false,
      posts: null,
    };
  },
  created() {
    this.fetchData();
  },
  methods: {
    fetchData() {
      this.loading = true;
      fetch("https://jsonplaceholder.typicode.com/posts")
        .then((response) => response.json())
        .then((json) => {
          this.loading = false;
          this.posts = json.slice(0, 10);
        });
      
    },
  },
});
